import React, { useState } from "react";
import QrReader from "react-qr-reader";
import swal from "sweetalert";
import "./App.css";

export default function App() {
  const [result, setResult] = useState(null);
  const [delay] = useState(500);

  const handleScan = (result) => {
    setResult(result);
  };
  const handleError = (err) => {
    console.error("err: ", err);
  };

  const previewStyle = {
    height: "100%",
    width: "100%",
  };

  if (result) {
    swal("Hasil Decode QR", result);
  }

  return (
    <div>
      <QrReader
        delay={delay}
        style={previewStyle}
        onError={handleError}
        onScan={handleScan}
      />
      <div>
        <h1 className='Text'>Simple QR Code Decoder</h1>
        <p className='Text'>
          A simple, lightweight application used to decode qrcode images in
          documents.
        </p>
      </div>
    </div>
  );
}
